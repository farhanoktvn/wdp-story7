from django.shortcuts import (
    render,
    redirect,
)
from story.forms import StatusForm
from story.models import Status

def index(request):
    if request.method == "POST":
        form = StatusForm(request.POST)
        if form.is_valid():
            s_name = form.cleaned_data['name']
            s_status = form.cleaned_data['status']
            response = {
                'name': s_name,
                'status': s_status,
            }
            return render(request, 'status-confirmation.html', response)
    else:
        form = StatusForm()
    status_list = Status.objects.all()
    response = {
        'status': status_list,
        'form': form,
    }
    return render(request, 'index.html', response)

def confirm(request):
    if request.method == "POST":
        form = StatusForm(request.POST)
        if form.is_valid():
            new_status = Status()
            new_status.name = request.POST['name']
            new_status.status = request.POST['status']
            new_status.save()
            return redirect('index')
    else:
        form = StatusForm()
    response = {
        'form': form,
    }
    return render(request, 'status-confirmation.html', response)
