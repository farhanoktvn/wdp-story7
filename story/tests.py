from django.test import TestCase, Client
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from story.models import Status
from story.forms import StatusForm
from story.views import index

import unittest
import time

class StoryTest(TestCase):

    def test_create_model_status(self):
        test_status = Status.objects.create(name="Marc", status="This is a status.")
        self.assertEqual(Status.objects.all().count(), 1)
        self.assertEqual(test_status.name, "Marc")
        self.assertEqual(test_status.status, "This is a status.")
        self.assertEqual(str(test_status), "This is a status.")

    def test_status_cannot_be_created_without_name(self):
        test_status = StatusForm({'name': '', 'status': 'sta'})
        self.assertFalse(test_status.is_valid())

    def test_status_cannot_be_created_without_status(self):
        test_status = StatusForm({'name': 'nam', 'status': ''})
        self.assertFalse(test_status.is_valid())

    def test_index_page(self):
        response = Client().get('')
        found = resolve('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')
        self.assertEqual(found.func, index)

    def test_confirmation_page(self) :
        response = Client().post('', {'name': 'name', 'status': 'stat'}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'status-confirmation.html')

    def test_confirmation_page_get_request(self) :
        response = Client().get('/confirm/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'status-confirmation.html')

    def test_form_is_not_valid(self) :
        response = Client().post('', {'name': '', 'status': ''}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')

    def test_confirmation_page_success_return_index(self) :
        response = Client().post('/confirm/', {'name': 'name_test', 'status': 'stat_test'}, follow=True)
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')
        self.assertIn('name_test', html_response)
        self.assertIn('stat_test', html_response)

    def test_confirmation_page_failure_stay(self) :
        response = Client().post('/confirm/', {'name': '', 'status': ''}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'status-confirmation.html')


class FunctionalTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument("--headless")
        self.browser = webdriver.Chrome(options=chrome_options)

    def tearDown(self):
        self.browser.quit()

    def test_input_form_revert_then_confirm(self):
        self.browser.get('http://localhost:8000/')

        """
        Test if we revert the confirmation
        """
        form_name = self.browser.find_element_by_name('name')
        form_stat = self.browser.find_element_by_name('status')
        form_submit = self.browser.find_element_by_class_name('submit-btn')

        form_name.send_keys('name_test')
        form_stat.send_keys('stat_test')
        form_submit.send_keys(Keys.RETURN)

        confirm_name = self.browser.find_element_by_name('name').get_attribute('value')
        confirm_stat = self.browser.find_element_by_name('status').get_attribute('value')
        confirm_button = self.browser.find_element_by_class_name('confirm-btn')
        confirm_revert = self.browser.find_element_by_class_name('revert-btn')

        self.assertIn('name_test', confirm_name)
        self.assertIn('stat_test', confirm_stat)
        confirm_revert.send_keys(Keys.RETURN)

        sorry_msg = self.browser.find_element_by_id('sorry-msg').text
        self.assertIn('no status', sorry_msg)

        """
        Test if we confirm the confirmation
        """
        form_name = self.browser.find_element_by_name('name')
        form_stat = self.browser.find_element_by_name('status')
        form_submit = self.browser.find_element_by_class_name('submit-btn')

        form_name.send_keys('name_test')
        form_stat.send_keys('stat_test')
        form_submit.send_keys(Keys.RETURN)

        confirm_name = self.browser.find_element_by_name('name').get_attribute('value')
        confirm_stat = self.browser.find_element_by_name('status').get_attribute('value')
        confirm_button = self.browser.find_element_by_class_name('confirm-btn')
        confirm_revert = self.browser.find_element_by_class_name('revert-btn')

        self.assertIn('name_test', confirm_name)
        self.assertIn('stat_test', confirm_stat)
        confirm_button.send_keys(Keys.RETURN)

        status_name = self.browser.find_element_by_class_name('card-header').text
        status_stat = self.browser.find_element_by_class_name('card-body').text
        self.assertIn('name_test', status_name)
        self.assertIn('stat_test', status_stat)

        """
        Test click change color
        """
        change_color = self.browser.find_element_by_class_name('change-color')
        change_color.send_keys(Keys.RETURN)

        head_color = self.browser.find_element_by_class_name('card-header').value_of_css_property('color')
        body_color = self.browser.find_element_by_class_name('card-body').value_of_css_property('color')

        self.assertEqual(head_color, "rgba(255, 0, 0, 1)")
        self.assertEqual(body_color, "rgba(0, 0, 255, 1)")
