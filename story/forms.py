from django import forms
from story.models import Status

class StatusForm(forms.Form):

    name = forms.CharField(widget=forms.TextInput(attrs={
        'type' : 'text',
        'class' : 'form-control',
        'maxlength' : '125',
        'required' : True,
        'placeholder' : 'Who are you?',
        'label': ''
    }))
    status = forms.CharField(widget=forms.TextInput(attrs={
        'type' : 'text',
        'class' : 'form-control',
        'maxlength' : '280',
        'required' : True,
        'placeholder' : 'What\'s on your mind?',
        'label': ''
    }))
