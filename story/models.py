from django.db import models

class Status(models.Model):

    name = models.CharField(max_length=125, null=False)
    status = models.CharField(max_length=280, null=False)

    def __str__(self):
        return self.status
